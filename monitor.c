#include "monitor.h"

#include <string.h>
#include <stdlib.h>

#include "errhandle.h"


Monitor_t createMonitor(void) {
    return (Monitor_t) {};
}

int initMonitor(Monitor_t *monitor) {
    pthread_mutexattr_t attributes;
    int errcode = pthread_mutexattr_init(&attributes);
    if (NO_ERROR != errcode) {
        errorfln("Failed to initialize attributes: %s", strerror(errcode));
        return errcode;
    }
    errcode = pthread_mutexattr_settype(&attributes, PTHREAD_MUTEX_ERRORCHECK);
    if (NO_ERROR != errcode) {
        errorfln("Failed to set mutex type");
        pthread_mutexattr_destroy(&attributes);
        return errcode;
    }
    errcode = pthread_mutex_init(&monitor->lock, &attributes);
    if (NO_ERROR != errcode) {
        errorfln("Cannot init mutex: %s", strerror(errcode));
        pthread_mutexattr_destroy(&attributes);
        return errcode;
    }
    pthread_mutexattr_destroy(&attributes);
    errcode = pthread_cond_init(&monitor->cv, NULL);
    if (NO_ERROR != errcode) {
        errorfln("Cannot init condition variable: %s", strerror(errcode));
        pthread_mutex_destroy(&monitor->lock);
        return errcode;
    }

    return NO_ERROR;
}

void destroyMonitor(Monitor_t * const monitor) {
    int errcode = pthread_mutex_destroy(&monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("Cannot destroy mutex: %s", strerror(errcode));
    }
    errcode = pthread_cond_destroy(&monitor->cv);
    if (NO_ERROR != errcode) {
        errorfln("Cannot destroy condition variable: %s", strerror(errcode));
    }
}

int wait_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_cond_wait(&monitor->cv, &monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to wait on monitor: %s", function, line, strerror(errcode));
    }
    return errcode;
}

void mustWait_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_cond_wait(&monitor->cv, &monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to wait on monitor: %s", function, line, strerror(errcode));
        exit(EXIT_FAILURE);
    }
}

int notify_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_cond_signal(&monitor->cv);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to notify on monitor: %s", function, line, strerror(errcode));
    }
    return errcode;
}

void mustNotify_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_cond_signal(&monitor->cv);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to notify on monitor: %s", function, line, strerror(errcode));
        exit(EXIT_FAILURE);
    }
}

int notifyAll_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_cond_broadcast(&monitor->cv);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to notify all on monitor: %s", function, line, strerror(errcode));
    }
    return errcode;
}

int lock_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_mutex_lock(&monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to lock monitor: %s", function, line, strerror(errcode));
    }
    return errcode;
}

int unlock_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_mutex_unlock(&monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to unlock monitor: %s", function, line, strerror(errcode));
    }
    return errcode;
}

void mustUnlock_(
        Monitor_t *monitor,
        const char *function,
        int line) {
    int errcode = pthread_mutex_unlock(&monitor->lock);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Failed to unlock monitor: %s", function, line, strerror(errcode));
        exit(EXIT_FAILURE);
    }
}

void mustUnlockSimple(void * const monitor) {
    Monitor_t *const m = (Monitor_t *) monitor;
    int errcode = pthread_mutex_unlock(&m->lock);
    if (NO_ERROR != errcode) {
        errorfln("Failed to unlock monitor: %s", strerror(errcode));
        exit(EXIT_FAILURE);
    }
}
