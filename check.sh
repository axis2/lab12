want_output="[parent] line #0
[child] line #0
[parent] line #1
[child] line #1
[parent] line #2
[child] line #2
[parent] line #3
[child] line #3
[parent] line #4
[child] line #4
[parent] line #5
[child] line #5
[parent] line #6
[child] line #6
[parent] line #7
[child] line #7
[parent] line #8
[child] line #8
[parent] line #9
[child] line #9"

executable=$1
niter=$2
verbose=$3

if [ "$executable" == "" ] || [ "$niter" == "" ]; then
  echo "usage: check.sh {executable to test} {number of iterations} [-v]"
  exit
fi

for (( i=1; i<=$niter; i++ ))
do
  got_output="$($executable)"
  if [ "$want_output" != "$got_output" ]; then
    echo "[$i] WRONG"
    echo "want:\n$want_output\ngot:\n$got_output"
    exit
  else
    if [ "$verbose" == "-v" ]; then
      echo "[$i] OK"
    fi
  fi
done

echo "OK"
