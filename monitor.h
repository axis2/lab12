#ifndef LAB12_MONITOR_H
#define LAB12_MONITOR_H

#include <pthread.h>


struct st_Monitor {
    pthread_mutex_t lock;
    pthread_cond_t cv;
};
typedef struct st_Monitor Monitor_t;

Monitor_t createMonitor(void);

int initMonitor(Monitor_t *monitor);

void destroyMonitor(Monitor_t *monitor);

// PSynchronizable is a pointer to so called "synchronizable object" -
// an object of any type that fits the following pattern:
// struct /* Name */ {
//     Monitor_t monitor;
//     /* other members */
// }
// where monitor must be initialized.
#define wait(PSynchronizable)          wait_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define mustWait(PSynchronizable)      mustWait_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define notify(PSynchronizable)        notify_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define mustNotify(PSynchronizable)    mustNotify_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define notifyAll(PSynchronizable)     notifyAll_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define lock(PSynchronizable)          lock_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define unlock(PSynchronizable)        unlock_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

#define mustUnlock(PSynchronizable)    mustUnlock_((Monitor_t *) (PSynchronizable), __FUNCTION__, __LINE__)

void mustUnlockSimple(void *monitor);

int wait_(
        Monitor_t *monitor,
        const char *function,
        int line);

void mustWait_(
        Monitor_t *monitor,
        const char *function,
        int line);

int notify_(
        Monitor_t *monitor,
        const char *function,
        int line);

void mustNotify_(
        Monitor_t *monitor,
        const char *function,
        int line);

int notifyAll_(
        Monitor_t *monitor,
        const char *function,
        int line);

int lock_(
        Monitor_t *monitor,
        const char *function,
        int line);

int unlock_(
        Monitor_t *monitor,
        const char *function,
        int line);

void mustUnlock_(
        Monitor_t *monitor,
        const char *function,
        int line);

#endif //LAB12_MONITOR_H
