#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include "errhandle.h"
#include "monitor.h"


enum en_Turn {
    PARENT,
    CHILD
};
typedef enum en_Turn Turn_t;

Turn_t otherTurn(const Turn_t turn) {
    return PARENT == turn ? CHILD : PARENT;
}

struct st_TurnHolder {
    Monitor_t monitor;
    Turn_t turn;
};
typedef struct st_TurnHolder TurnHolder_t;

void passTurn(TurnHolder_t *const p_turn_holder) {
    p_turn_holder->turn = otherTurn(p_turn_holder->turn);
}

#define LINES_COUNT                 10
#define PARENT_PREFIX               "[parent]"
#define CHILD_PREFIX                "[child]"

struct st_PrintArguments {
    char const *const prefix;
    pthread_t *const p_other_thread;
    const Turn_t own_turn;
    TurnHolder_t *const p_turn_holder;
};
typedef struct st_PrintArguments PrintArguments_t;

void mustCancel(const pthread_t *const p_thread) {
    errorfln("Cancelling requested thread");
    int errcode = pthread_cancel(*p_thread);
    if (NO_ERROR != errcode) {
        errorfln("Cannot cancel requested thread: %s", strerror(errcode));
        exit(EXIT_FAILURE);
    }
}

void *printLines(void *v_args) {
    if (NULL == v_args) {
        ferrorfln("Unexpected argument value: null");
        return NULL;
    }
    const PrintArguments_t *const args = (PrintArguments_t *) v_args;
    if (NULL == args->prefix) {
        ferrorfln("No prefix provided");
        return NULL;
    }
    if (NULL == args->p_turn_holder) {
        ferrorfln("No turn holder provided");
        return NULL;
    }
    if (NULL == args->p_other_thread) {
        ferrorfln("No other thread pointer provided");
        return NULL;
    }

    const Turn_t own_turn = args->own_turn;
    TurnHolder_t *const p_turn_holder = args->p_turn_holder;

    int errcode;

    pthread_cleanup_push(mustUnlockSimple, (void *) p_turn_holder);
    for (int i = 0; i < LINES_COUNT; i += 1) {
        errcode = lock(p_turn_holder);
        if (NO_ERROR != errcode) {
            if (EDEADLOCK == errcode) {
                mustUnlock(p_turn_holder);
            }
            mustCancel(args->p_other_thread);
            return NULL;
        }

        while (p_turn_holder->turn != own_turn) {
            mustWait(p_turn_holder);
        }
        printf("%s line #%d\n", args->prefix, i);

        passTurn(p_turn_holder);
        mustNotify(p_turn_holder);

        mustUnlock(p_turn_holder);
    }
    pthread_cleanup_pop(false);

    return NULL;
}

int main() {
    int errcode;
    TurnHolder_t turn_holder = { .monitor = createMonitor(), .turn = PARENT };
    errcode = initMonitor(&turn_holder.monitor);
    if (NO_ERROR != errcode) {
        return EXIT_FAILURE;
    }

    pthread_t thread;
    pthread_t self = pthread_self();
    errcode = pthread_create(&thread, NULL, printLines, &(PrintArguments_t) {
            .p_other_thread = &self,
            .prefix = CHILD_PREFIX,
            .own_turn = CHILD,
            .p_turn_holder = &turn_holder
    });
    if (NO_ERROR != errcode) {
        errorfln("Failed to spawn a thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    printLines(&(PrintArguments_t) {
            .p_other_thread = &thread,
            .prefix = PARENT_PREFIX,
            .own_turn = PARENT,
            .p_turn_holder = &turn_holder
    });

    errcode = pthread_join(thread, NULL);
    if (NO_ERROR != errcode) {
        errorfln("Failed to join child thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    destroyMonitor(&turn_holder.monitor);

    return EXIT_SUCCESS;
}
